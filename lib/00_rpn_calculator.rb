class RPNCalculator
  def initialize
    @stack = []
  end 
  
  def push(val)
    @stack << val 
  end 
  
  def check_size 
    raise "calculator is empty" if @stack.length < 2 
  end 
  
  def plus
    check_size
    a = @stack.pop
    b = @stack.pop 
    @stack << a + b
  end 
  
  def minus 
    check_size
    a = @stack.pop 
    b = @stack.pop  
    @stack << b - a
  end 
  
  def times
    check_size
    a = @stack.pop 
    b = @stack.pop  
    @stack << b * a
  end 
  
  def divide 
    check_size
    a = @stack.pop 
    b = @stack.pop  
    @stack << b / a.to_f
  end 
  
  def value 
    @stack.last 
  end 
  
  OPERATORS = {"+" => :+, "-" => :-, "*" => :*, "/" => :/ }
  
  def tokens(str)
    str.split.map do |el|
      if OPERATORS.include?(el)
        OPERATORS[el]
      else 
        el.to_i 
      end 
    end 
  end 
  
  def evaluate(str)
    tokens(str).each do |token|
      if token.is_a? Symbol 
        plus if token == :+ 
        minus if token == :- 
        times if token == :* 
        divide if token == :/ 
      else
        push(token.to_i)
      end 
    end
    value 
  end 
end
